package com.example.capsula.reportapetrabajadorprueba.presentation.presenters;

import android.content.Context;

import com.example.capsula.reportapetrabajadorprueba.data.entities.Reporte;
import com.example.capsula.reportapetrabajadorprueba.data.entities.ReporteUsuario;
import com.example.capsula.reportapetrabajadorprueba.data.repositories.local.SessionManager;
import com.example.capsula.reportapetrabajadorprueba.data.repositories.remote.ServiceGeneratorSimple;
import com.example.capsula.reportapetrabajadorprueba.data.repositories.remote.request.FotoRequest;
import com.example.capsula.reportapetrabajadorprueba.data.repositories.remote.request.ReporteRequest;
import com.example.capsula.reportapetrabajadorprueba.presentation.contracts.VerFotosContract;
import com.example.capsula.reportapetrabajadorprueba.presentation.contracts.VerReportesContract;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Desarrollo3 on 2/03/2017.
 */

public class VerFotoPresenter implements VerFotosContract.Presenter {

    private VerFotosContract.View mView;
    Context context;
    SessionManager sessionManager;

    public VerFotoPresenter(VerFotosContract.View mView, Context context) {
        this.mView = mView;
        this.context = context;
        this.mView.setPresenter(this);

        //sessionManager = new SessionManager(context);
    }



    @Override
    public void start() {
        sessionManager = new SessionManager(context);
        System.out.println("ID REPORTE GRUPAL: "+ sessionManager.getIdRG());
        ServiceGeneratorSimple serviceGeneratorSimple = new ServiceGeneratorSimple();
        Gson gsonDate = serviceGeneratorSimple.construyeGsonDeserializador();
        FotoRequest fotoRequest = serviceGeneratorSimple.ConexionRestApi(gsonDate);
        Call<ReporteUsuario> call = fotoRequest.obtenerFotosReporte(sessionManager.getIdRG());
        call.enqueue(new Callback<ReporteUsuario>() {
            @Override
            public void onResponse(Call<ReporteUsuario> call, Response<ReporteUsuario> response) {
                if (!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                if(response.isSuccessful()){
                    System.out.println(call+ "    ");
                    System.out.println(response);
                    mView.mostrarFotos(response.body().datos);
                }else {
                    mView.verFotosDenegado("Hubo un error al mostrar la lista");
                }

            }

            @Override
            public void onFailure(Call<ReporteUsuario> call, Throwable t) {
                System.out.println("F A L L A ---> " + call);
                System.out.println(t);
                if (!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.verFotosDenegado("No se pudo acceder al servidor en este momento");
            }
        });
    }
}

   /* @Override
    public void obtenerFoto(int reporteId) {
        sessionManager = new SessionManager(context);
        System.out.println("ID: "+ sessionManager.getIdRG());
        ServiceGeneratorSimple serviceGeneratorSimple = new ServiceGeneratorSimple();
        Gson gsonDate = serviceGeneratorSimple.construyeGsonDeserializador();
        FotoRequest fotoRequest = serviceGeneratorSimple.ConexionRestApi(gsonDate);
        Call<ReporteUsuario> call = fotoRequest.obtenerFotosReporte(sessionManager.getIdRG());
        //Call<Reporte> call = reporteRequest.obtenerReporteGrupal(sessionManager.getId());
        call.enqueue(new Callback<ReporteUsuario>() {
            @Override
            public void onResponse(Call<ReporteUsuario> call, Response<ReporteUsuario> response) {
                if (!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                if(response.isSuccessful()){
                    System.out.println(call+ "    ");
                    System.out.println(response);
                    mView.mostrarFotos(response.body().datos);
                }else {
                    mView.verFotosDenegado("Hubo un error al mostrar la lista");
                }

            }

            @Override
            public void onFailure(Call<ReporteUsuario> call, Throwable t) {
                System.out.println("F A L L A ---> " + call);
                System.out.println(t);
                if (!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.verFotosDenegado("No se pudo acceder al servidor en este momento");
            }
        });
    }*/


