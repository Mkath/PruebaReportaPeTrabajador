package com.example.capsula.reportapetrabajadorprueba.presentation.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.capsula.reportapetrabajadorprueba.R;
import com.example.capsula.reportapetrabajadorprueba.data.entities.ReporteSemaforo;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Desarrollo3 on 2/03/2017.
 */

public class VerFotosAdapter extends RecyclerView.Adapter<VerFotosAdapter.ReporteUsuarioViewHolder> {

    ArrayList<ReporteSemaforo> reporteUser;
    Activity activity;
    @BindView(R.id.imag_foto)
    ImageView imagFoto;
    @BindView(R.id.fecha_foto)
    TextView fechaFoto;
    @BindView(R.id.cardview_rs)
    CardView cardviewRs;

    public VerFotosAdapter(ArrayList<ReporteSemaforo> reportes, Activity activity) {
        this.reporteUser = reportes;
        this.activity = activity;
    }

    @Override
    public ReporteUsuarioViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_fotos, parent, false);
        //Esta linea de codigo asocia el layour cardview_contacto con el ReportesActivity
        return new ReporteUsuarioViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ReporteUsuarioViewHolder holder, int position) {
        //Aca pasamos la lista de reportes
        final ReporteSemaforo reporte = reporteUser.get(position);
        holder.imagFotos.setImageBitmap(StringToBitMap(reporte.reporteusuario_foto));
        holder.fechaFoto.setText(reporte.reporteusuario_fechahora);
        holder.imagFotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final RelativeLayout root = new RelativeLayout(activity);
                root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
                final Dialog dialog = new Dialog(activity);
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.activity_detalle_foto);
                // set the custom dialog components - text, image and button
                ImageView image = (ImageView) dialog.findViewById(R.id.imgFotoDetalle);
                image.setImageBitmap(StringToBitMap(reporte.reporteusuario_foto));
                // if button is clicked, close the custom dialog
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return reporteUser.size();
    }

    @OnClick(R.id.imag_foto)
    public void onClick() {
    }


    public static class ReporteUsuarioViewHolder extends RecyclerView.ViewHolder {
        //Esta clase es la que interacciona con el layout
        private TextView fechaFoto;
        private ImageView imagFotos;

        public ReporteUsuarioViewHolder(final View itemView) {
            super(itemView);
            fechaFoto = (TextView) itemView.findViewById(R.id.fecha_foto);
            imagFotos = (ImageView) itemView.findViewById(R.id.imag_foto);
        }

        }
    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }
}


