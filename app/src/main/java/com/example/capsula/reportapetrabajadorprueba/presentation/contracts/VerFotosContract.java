package com.example.capsula.reportapetrabajadorprueba.presentation.contracts;

import com.example.capsula.reportapetrabajadorprueba.core.BasePresenter;
import com.example.capsula.reportapetrabajadorprueba.core.BaseView;
import com.example.capsula.reportapetrabajadorprueba.data.entities.ReporteGrupal;
import com.example.capsula.reportapetrabajadorprueba.data.entities.ReporteSemaforo;

import java.util.ArrayList;

/**
 * Created by Desarrollo3 on 2/03/2017.
 */

public interface VerFotosContract {
    interface View extends BaseView<Presenter> {
        void mostrarFotos(ArrayList<ReporteSemaforo> reporteUser);
        void verFotosDenegado(String response);
    }
    interface Presenter extends BasePresenter {
        //void obtenerFoto(int reporteId);
    }
}
