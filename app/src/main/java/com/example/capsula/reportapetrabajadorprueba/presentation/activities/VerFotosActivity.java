package com.example.capsula.reportapetrabajadorprueba.presentation.activities;
import android.os.Bundle;
import com.example.capsula.reportapetrabajadorprueba.R;
import com.example.capsula.reportapetrabajadorprueba.core.BaseActivity;
import com.example.capsula.reportapetrabajadorprueba.presentation.fragments.VerFotosFragment;
import com.example.capsula.reportapetrabajadorprueba.presentation.presenters.VerFotoPresenter;
import com.example.capsula.reportapetrabajadorprueba.utils.ActivityUtils;

/**
 * Created by Desarrollo3 on 2/03/2017.
 */

public class VerFotosActivity extends BaseActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("vista reportes creada");
        setContentView(R.layout.activity_fotos);
        VerFotosFragment fragment = (VerFotosFragment) getSupportFragmentManager().findFragmentById(R.id.body_fotos);
        if (fragment == null){
            fragment = VerFotosFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),fragment,R.id.body_fotos);
        }

        new VerFotoPresenter(fragment , getApplicationContext());

    }
}
